# LSF Ansible #

Ansible playbook and roles for managing Libre Space Foundation infrastructure.

## Requirements ##

1. Ansible 2.4
2. GnuPG
3. Vagrant (testing)

## Usage ##

### Test ###

To test the playbook:
```
$ vagrant up
```

If Vagrant is already up:
```
$ vagrant provision
```

### Deploy ###

To deploy the playbook on the staging environment:
```
$ ansible-playbook -i staging/inventory site.yml
```

To deploy the playbook on the production environment:
```
$ ansible-playbook -i production/inventory site.yml
```

## License

[![license](https://img.shields.io/badge/AGPLv3-6672DB.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202018-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
